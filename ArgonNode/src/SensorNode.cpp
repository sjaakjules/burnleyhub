#include "application.h"
#line 1 "c:/Users/julia/Documents/Particle/projects/GitProjects/BurnleyHub/TerrariumNode/src/SensorNode.ino"
/*
* Project SensorNode
* Description: Main script
* Author:  Julian Rutten - jrutten@swin.edu.au
* Date:    11/07/2018
*/

#include "SensorController.h"
#include "SensorBase.h"
//#include "SDwritter.h"
#include "SPI.h"
#include "SdFat.h"

void writeHeader();
void logData();
void trySetupSDCard();
void setup_Options();
void setup();
void loop();
void tryToSaveSensorInfo();
void saveSensorInfo();
void publishMyData();
void cancelWiFiCallback();
void cancelWiFi();
void tryConnectWifi();
void tryConnectCloud();
void Add3GWifi();
void wifi_scan_callback(WiFiAccessPoint *wap, void *data);
void mycustomScan();
void goToSleep();
bool getEEPROMdata();
void updateName();
void setNewName(String newName);
void handler(const char *topic, const char *data);
void printSensorInfo();
String addSensorValue(String message, String code, String Value);
void sendMessage(String message, String DeviceName, String WebHookName);
#line 14 "c:/Users/julia/Documents/Particle/projects/GitProjects/BurnleyHub/TerrariumNode/src/SensorNode.ino"
SYSTEM_MODE(SEMI_AUTOMATIC);
STARTUP(setup_Options());

struct EEPROMinfo
{
  uint8_t flag;
  char name[24];

  EEPROMinfo(String newname) : flag(0), name("")
  {
    if (newname.length() < 24)
    {
      for (size_t i = 0; i < newname.length(); i++)
      {
        name[i] = newname.charAt(i);
      }
    }
    else
    {
      for (size_t i = 0; i < 24; i++)
      {
        name[i] = newname.charAt(i);
      }
    }
  }

  EEPROMinfo() : flag(0)
  {
    name[0] = '\0';
  }
};

//////////////////////////////////////////////////////////////
//            THE SAVING SETTINGS
//////////////////////////////////////////////////////////////
bool isDebugMode = true;
bool forceSaveSD = false;
bool isSavingSD = true;
bool isCloudLoop = true;
const int nSaves = 10;

String WebHookName = "ThermoDemo";

//////////////////////////////////////////////////////////////
//            WIFI AND TIMER SETTINGS
//////////////////////////////////////////////////////////////
int sleepDuration = 600;
int cloudConnectionDuration = 5000;
int wifiConnectionDuration = 5000;
int wiFiConnectionAttemptMAX = 3;
int interval = 30;      // interval in minutes to wake with first one at O'clock.
int intervalBuffer = 1; // This sets number of minutes +- either side of interval.

SensorController sensors;
const int nStaticReadings = 18; // DO NOT EDIT! THIS MATCHES CONNECTED SENSORS
int counter = 0;
String DeviceName = "";
bool isSeadingMessage = false;
bool sentMessage = false;
bool waitingForName = false;

unsigned long firstAvailable = 0;
bool wifiReady = false;
bool cloudReady = false;
bool connectingToCloud = false;
bool isUsingPowerPack = false;
bool earlyShutdown = false;
long timeStart = 0;

int wiFiConnectionAttempts = 0;

//int NPN_Pin = D4;
//int PNP_Pin = D5;

//WIFI CREDENTIALS for development
/*DO NOT CHANGE*/ const char *SSID1 = "3G uFi_61B";
/*DO NOT CHANGE*/ const char *SSID2 = "3G uFi_9E3";
/*DO NOT CHANGE*/ const char *SSID3 = "3G uFi_C83";
/*DO NOT CHANGE*/ const char *SSID4 = "3G uFi_C6B";
/*DO NOT CHANGE*/ const char *SSID5 = "3G uFi_9E0";
/*DO NOT CHANGE*/ const char *PASS = "4Qiud29-da";

retained int nReadingsSaved = 0;
retained bool haveGotName = false;
retained char deviceNameRet[25] = "Unknown";
int Readings[nSaves][nStaticReadings];
retained char JSONcode[nStaticReadings][3];

retained bool hasAddedNewSSID = false;

EEPROMinfo ActiveEEPROMData; // = {0, "Unknown"};
int deviceNumberEEPROMAddress = 0;

//String[] allDeviceNames= {"Ardvak", "Badger", "Cougar", "Donkey", "Earwig", "Falcon", "Gerbil", "Horsey", "Iguana", "Jaguar", "Kakapo", "Lobsta", "Magpie",
//"Numbat", "Oyster", "Parrot", "Quolls", "Rabbit", "Shrimp", "Turkey", "Uakari", "Vultur", "Walrus", "Xantus", "Yettie", "Zonkey"};

//////////////////////////////////////////////////////////////
//            SD CARD READ/WRITE CONTENT
//////////////////////////////////////////////////////////////
// SD chip select pin.  Be sure to disable any other SPI devices such as Enet.
const uint8_t chipSelect = SS;
// Log file base name.  Must be six characters or less.
#define FILE_BASE_NAME "Data"

const uint8_t BASE_NAME_SIZE = sizeof(FILE_BASE_NAME) - 1;
char fileName[13] = FILE_BASE_NAME "00.csv";
//------------------------------------------------------------------------------
// File system object.
SdFat sd;
// Log file.
SdFile file;

// Write data header.
void writeHeader()
{
  for (size_t j = 0; j < nStaticReadings; j++)
  {
    file.print("," + String(JSONcode[j]));
  }

  file.print("Dv");
  file.println();
}
//------------------------------------------------------------------------------
// Log a data record.
void logData()
{
    //  Serial.println("Memory in upload   : " +  String(System.freeMemory()));
    for (size_t j = 0; j < nSaves; j++)
    {
      for (size_t i = 0; i < nStaticReadings; i++)
      {
        file.print(String(Readings[j][i] * 1.0f / 1000, 3) + ",");
      }
      file.print(String(deviceNameRet));

      file.println();
      delay(1000);
    }
    file.close();

}

//==============================================================================
// Error messages stored in flash.
#define error(msg) sd.errorHalt(F(msg))

void trySetupSDCard()
{
  if (isSavingSD)
  {
    // Initialize at the highest speed supported by the board that is
    // not over 50 MHz. Try a lower speed if SPI errors occur.
    if (!sd.begin(chipSelect, SD_SCK_MHZ(50)))
    {
      sd.initErrorHalt();
    }

    // Check if file exists and file size.
    // If it doesnt exist or too large create a new one.
    if (sd.exists(fileName))
    {
      if (!file.open(fileName, O_WRONLY | O_CREAT | O_EXCL))
      {
        error("file.open");
      }
      else
      {
        if (file.fileSize() > 4000000000)
        {
          while (sd.exists(fileName))
          {
            if (fileName[BASE_NAME_SIZE + 1] != '9')
            {
              fileName[BASE_NAME_SIZE + 1]++;
            }
            else if (fileName[BASE_NAME_SIZE] != '9')
            {
              fileName[BASE_NAME_SIZE + 1] = '0';
              fileName[BASE_NAME_SIZE]++;
            }
            else
            {
              error("Can't create file name");
            }
          }
        }
      }
    }

    if (!file.open(fileName, O_WRONLY | O_CREAT | O_EXCL))
    {
      error("file.open");
    }

    writeHeader();
    // else card is setup.
    file.close();
  }
}

Timer cloudConnectionTimer(cloudConnectionDuration, cancelWiFiCallback, true);

void setup_Options()
{
  WiFi.selectAntenna(ANT_AUTO);
  System.enableFeature(FEATURE_RETAINED_MEMORY);
  System.enableFeature(FEATURE_RESET_INFO);
}

// setup() runs once, when the device is first turned on.
void setup()
{
  Serial.begin(9600);
  if (isDebugMode)
  {
    while (!Serial.isConnected())
      Particle.process();
  }

  Serial.println("Running Setup for loop, " + String(nReadingsSaved));

  if (forceSaveSD)
  {
    /* The Wifi should be off and it will not be a cloud loop. */
    cancelWiFi();
    isSavingSD = true;
  }
  else
  {
    //isCloudLoop = true; isSavingSD = false;
  }

  // Will turn off if it wakes up outside of data capture time. To ensure all is synced
  if (!isDebugMode && ((Time.minute() + intervalBuffer) % interval) - 2 * intervalBuffer > 0)
  {
    goToSleep();
  }

  trySetupSDCard();
  tryConnectCloud();
  updateName();

  Serial.println("Running Setup for loop, " + String(nReadingsSaved));
  Serial.println("Device name: " + String(deviceNameRet));

  Time.zone(+10);
  Serial.println("Connecting the sensors...");

  sensors.SetupSensors();
  Serial.println("Sensors Connected, now to loop...");

  timeStart = millis();
}

// loop() runs over and over again, as quickly as it can execute.
void loop()
{

  Serial.println("Mr loop has occured..");
  //  Go to sleep if you have been looping for a while...
  if (millis() - timeStart > 120000)
  {
    goToSleep();
  }

  // Try and connect...
  tryConnectCloud();
  updateName();

  tryToSaveSensorInfo();

  delay(1000);
}

//////////////////////////////////////////////////////////////
//            SENSOR READING FUNCTIONS
//////////////////////////////////////////////////////////////

void tryToSaveSensorInfo()
{
  bool hasSavedReadings = false;
  // Flush sensors...
  sensors.UpdateSensors();
  delay(50);
  sensors.UpdateSensors();
  delay(50);

  // Save multiple readings
  nReadingsSaved = 0;
  for (size_t i = 0; i < nSaves; i++)
  {
    sensors.UpdateSensors();
    saveSensorInfo();
    nReadingsSaved++;
    delay(50);
  }

  if (haveGotName && isSavingSD)
  {
    // TODO write to SD card
    printSensorInfo();
    logData();
    hasSavedReadings = true;
  }
  if (haveGotName && cloudReady)
  {
    // We are connected to the internet! lets dance!
    //Serial.println("We are connected, now to upload!!");
    //sensors.UploadSensors();
    publishMyData();
    //Serial.println("Finsihed loop, " + String(nReadingsSaved) + "Now time for sleep...");
    if (nReadingsSaved >= nSaves)
    {
      nReadingsSaved = 0;
    }
    hasSavedReadings = true;
  }

  if (hasSavedReadings)
  {
    goToSleep();
  }
}

// This fuction will save info to Readings variable.
// Assumed that nReadingsSaved is updated
void saveSensorInfo()
{
  String tempCode = "--";
  for (size_t i = 0; i < nStaticReadings; i++)
  {
    tempCode = String(sensors.getJsonCodes(i));
    JSONcode[i][0] = tempCode.charAt(0);
    JSONcode[i][1] = tempCode.charAt(1);
    JSONcode[i][2] = '\0';

    if (tempCode == "Sc")
    {
      Readings[nReadingsSaved][i] = Time.second() * 1000;
    }
    else
    {
      Readings[nReadingsSaved][i] = (sensors.getLatestData())[i] * 1000;
    }
  }
}

//
void publishMyData()
{
  //  Serial.println("Memory in upload   : " +  String(System.freeMemory()));
  for (size_t j = 0; j < nSaves; j++)
  {
    String message = "";
    message.concat(String("{ "));

    for (size_t i = 0; i < nStaticReadings; i++)
    {
      message.concat(String("\"" + String(JSONcode[i]) + "\":" + String(Readings[j][i] * 1.0f / 1000, 3) + ", "));
    }
    message.concat("\"Dv\": \"" + String(deviceNameRet) + "\" }");
    Particle.publish(WebHookName, String(message), 60, PUBLIC); // WebHook to Google Sheets

    delay(1000);
    Serial.println("Sending to the cloud... ");
    Serial.println(message);
    //  delete message;
  }
  nReadingsSaved = 0;
}

//////////////////////////////////////////////////////////////
//            WIFI AND CONNECTION FUNCTIONS
//////////////////////////////////////////////////////////////
void cancelWiFiCallback()
{
  if (connectingToCloud && !cloudReady)
  {

    Serial.println("Took too long... will cancel Wifi and the cloud... ");
    cancelWiFi();
  }
}

void cancelWiFi()
{
  WiFi.disconnect();
  WiFi.off();

  isSavingSD = true;
  isCloudLoop = false;
  wifiReady = false;
  cloudReady = false;
  connectingToCloud = false;
}

void tryConnectWifi()
{
  if (wiFiConnectionAttempts < wiFiConnectionAttemptMAX)
  {
    WiFi.on();
    Serial.println("Connecting WiFi...");
    if (!hasAddedNewSSID)
    {
      mycustomScan();
    }
    WiFi.connect();
    //Serial.println("Connecting to Cloud...");                                     ///////////////////// UPDATE
    tryConnectCloud();
  }
  else
  {
    cancelWiFi();
  }
}

void tryConnectCloud()
{
  if (isCloudLoop && !cloudReady)
  {
    if (WiFi.ready() || WiFi.connecting())
    {
      if (waitFor(WiFi.ready, wifiConnectionDuration))
      {
        Serial.println("Wifi is ready. now to connect to the cloud. ");
        wifiReady = true;
        cloudConnectionTimer.start();
        connectingToCloud = true;

        Serial.println("Connecting to the cloud... ");
        if (Particle.connected() == false)
        {
          Particle.connect();
        }
        if (isCloudLoop)
        {
          cloudReady = Particle.connected();
          connectingToCloud = false;
          wiFiConnectionAttempts = 0;

          Serial.println("We are connected to the cloud. Hazar");
        }
      }
    }
    else
    {
      wiFiConnectionAttempts++;
      tryConnectWifi();
    }
  }
}

void Add3GWifi()
{
  WiFi.scan(wifi_scan_callback);
}

void wifi_scan_callback(WiFiAccessPoint *wap, void *data)
{
  WiFiAccessPoint &ap = *wap;
  if (ap.ssid[0] == '3' && ap.ssid[1] == 'G')
  {
    Serial.print("SSID: ");
    Serial.println(ap.ssid);
    WiFi.setCredentials(ap.ssid, PASS);
    hasAddedNewSSID = true;
  }
  else
  {

    Serial.print("Unknown SSID: ");
    Serial.println(ap.ssid);
  }
}

void mycustomScan()
{
  WiFiAccessPoint aps[20];
  WiFiAccessPoint Myap[5];
  int foundMy = WiFi.getCredentials(Myap, 5);
  int found = WiFi.scan(aps, 20);
  for (int i = 0; i < found; i++)
  {
    WiFiAccessPoint &ap = aps[i];
    if (ap.ssid[0] == '3' && ap.ssid[1] == 'G')
    {
      bool haveGot = false;

      for (int j = 0; j < foundMy; j++)
      {
        if (Myap[j].ssid == ap.ssid)
        {
          haveGot = true;
          break;
        }
      }
      if (!haveGot)
      {
        Serial.print("SSID: ");
        Serial.println(ap.ssid);
        WiFi.setCredentials(ap.ssid, PASS);
        hasAddedNewSSID = true;
      }
      else
      {
        Serial.print("I already know SSID: ");
        Serial.println(ap.ssid);
      }
    }
    else
    {

      Serial.print("Unknown SSID: ");
      Serial.println(ap.ssid);
    }
  }
}

void goToSleep()
{
  int min = Time.minute();
  int seconds = Time.second();
  int secondsTillNextInterval = (interval - (min % interval) - 1) * 60 + 60 - seconds;

  Serial.end();
  System.sleep(SLEEP_MODE_DEEP, secondsTillNextInterval);
}

//////////////////////////////////////////////////////////////
//            DEVICE NAME FUNCTIONS
//////////////////////////////////////////////////////////////
void updateEEPROM(EEPROMinfo newData)
{
  EEPROMinfo EEPROMdata;
  EEPROM.get(deviceNumberEEPROMAddress, EEPROMdata);

  Serial.println("Got info from EEPROM. Now checking.");
  if (EEPROMdata.flag != 0)
  {
    // EEPROM was empty -> initialize myObj

    Serial.println("Data in EEPROM is not initialized. Putting new data.");

    EEPROM.put(deviceNumberEEPROMAddress, newData);
    Serial.println("Data Put...");
  }
  else
  {
    if (String(newData.name) != String(EEPROMdata.name))
    {
      Serial.println("Data exists but not the same as current name.");
      Serial.println("NAme in EEPROM: " + String(EEPROMdata.name));
      Serial.println("new name      : " + String(newData.name));

      EEPROM.put(deviceNumberEEPROMAddress, newData);
      Serial.println("Data Put...");
    }
  }
  Serial.println("Set new EEPROM data, " + String(EEPROMdata.flag) + ": " + EEPROMdata.name);
  ActiveEEPROMData = newData;
}

bool getEEPROMdata()
{
  EEPROMinfo EEPROMdata;
  EEPROM.get(deviceNumberEEPROMAddress, EEPROMdata);
  if (EEPROMdata.flag != 0)
  {
    //// EEPROM was empty

    Serial.println("EEPROM was empty");
    return false;
  }
  ActiveEEPROMData = EEPROMdata;

  Serial.println("Found EEPROM data, " + String(EEPROMdata.flag) + ": " + EEPROMdata.name);
  return true;
}

void setNameOfEEPROM(EEPROMinfo newName)
{
  Serial.println("NAme to save to EEPROM: " + String(newName.name));
  String Shotname;
  if (String(newName.name).length() > 23)
  {
    Shotname = String(newName.name).substring(0, 22);
  }
  else
  {
    Shotname = String(newName.name);
  }

  Serial.println("Name shortened to: " + Shotname);
  EEPROMinfo newData = EEPROMinfo(Shotname);
  updateEEPROM(newData);
}

void updateName()
{
  if (!haveGotName)
  {

    Serial.println("No name, Will try and get one...");
    if (isCloudLoop)
    {
      if (cloudReady && !waitingForName)
      {
        Serial.println("We dont have a device name. I will register with the cloud...");
        Particle.subscribe("particle/device/name", handler, ALL_DEVICES);
        Particle.publish("particle/device/name", PUBLIC);
        Serial.println("Waiting to get device name...");
        waitingForName = true;
      }
      else if (waitingForName)
      {
        Serial.println("Still waiting for a name...");
      }
    }
    else
    {
      if (getEEPROMdata())
      {
        Serial.println("Got name from EEPROM!");
        // got EEPROM data, can update name.
        setNewName(String(ActiveEEPROMData.name));
      }
      else
      {
        Serial.println("Not a cloud loop and no saved name, I call thee NoName...");
        String tempName = "NoName";
        setNewName(tempName);
        Serial.println("We have a name: " + String(deviceNameRet));
      }
    }
  }
}

void setNewName(String newName)
{
  DeviceName = String(newName);
  sensors.ComName = String(newName);
  if (newName.length() < 24)
  {
    for (size_t i = 0; i < newName.length(); i++)
    {
      deviceNameRet[i] = newName.charAt(i);
    }
    if (DeviceName.length() < 7)
    {
      for (size_t i = DeviceName.length(); i < 7; i++)
      {
        deviceNameRet[i] = '\0';
      }
    }
  }
  else
  {
    for (size_t i = 0; i < 24; i++)
    {
      deviceNameRet[i] = newName.charAt(i);
    }
  }
  haveGotName = true;
}

// This is the call back to get device name.
void handler(const char *topic, const char *data)
{
  Serial.println("Call back for device name!");
  if (!haveGotName)
  {
    String loadedNAme = String(data);
    Serial.println("received " + String(topic) + ": " + loadedNAme);
    Serial.println("Saving name...");

    Serial.println("Setting new name...: " + loadedNAme);
    setNewName(loadedNAme);
    Serial.println("Set name, now saving to EEPROM...");
    EEPROMinfo Name = EEPROMinfo(loadedNAme);
    setNameOfEEPROM(Name);
    Serial.println("We have a name in ram    : " + String(deviceNameRet));
    Serial.println("We have a name in on file: " + String(DeviceName));
    Serial.println("We have a name in EEPROM : " + String(ActiveEEPROMData.name));
  }
}
//////////////////////////////////////////////////////////////
//            UNUSED FUNCTIONS
//////////////////////////////////////////////////////////////

// Prints to serial...
void printSensorInfo()
{
  for (size_t i = 0; i <= nReadingsSaved; i++)
  {
    Serial.printf("Loop %i :", i);
    for (size_t j = 0; j < nStaticReadings; j++)
    {
      Serial.print(" : " + String(JSONcode[j]));
      Serial.print(" : " + String(Readings[i][j] * 1.0f / 1000));
    }
    Serial.println("");
  }
}
// To round values to 4 decimal places use String(Value,4)
// The Codes are teh EXACT same 2 letter codes in the webhook integration.

String addSensorValue(String message, String code, String Value)
{

  message.concat(String("\"" + code + "\": " + Value + ", "));
  return message;
}

void sendMessage(String message, String DeviceName, String WebHookName)
{
  String outMessage = "";
  outMessage.concat(String("{ "));
  outMessage.concat(String("\"Dv\": " + DeviceName + ", "));
  outMessage.concat(message);
  outMessage.concat("\"Dv\": " + DeviceName + ", ");
  outMessage.concat("\"Yr\": " + String(Time.year()) + ", ");
  outMessage.concat("\"Mo\": " + String(Time.month()) + ", ");
  outMessage.concat("\"Da\": " + String(Time.day()) + ", ");
  outMessage.concat("\"Hr\": " + String(Time.hour()) + ", ");
  outMessage.concat("\"Mi\": " + String(Time.minute()) + ", ");
  outMessage.concat("\"Sc\": " + String(Time.second()) + ", ");
  outMessage.concat("\"Zo\": " + String(10) + " }");

  if (Particle.connected)
  {
    Particle.publish(WebHookName, outMessage, 60, PUBLIC);
  }
}

//////////////////////////////////////////////////////////////
//            OLD LOOP FUNCTIONS
//////////////////////////////////////////////////////////////

/*
sensors.UpdateSensors();
delay(50);
sensors.UpdateSensors();
delay(50);

for (size_t i = 0; i < nSaves; i++) {
sensors.UpdateSensors();
saveSensorInfo();
//Serial.println("Finsihed loop " + String(nReadingsSaved) + ". Not a cloud loop, so time for sleep...");
nReadingsSaved++;
delay(500);
}

if (isCloudLoop) {
//Serial.println("Loop should be connected to the cloud...");
if (!wifiReady) {
//Serial.println(WiFi.connecting()?"Connecting to Wifi... stand by...":"Not connecting...");
if (!WiFi.connecting()) {
//Serial.println("Wifi is not on, or not connecting..");
WiFi.on();
if (!hasAddedNewSSID) {
mycustomScan();
}
WiFi.connect();
//Serial.println("Now on and connecting...");
}
}

// Connect to the cloud. This happens if we havent already tried to connect.
if (!connectingToCloud && waitFor(WiFi.ready, 1000)) {

//Serial.println("Starting connection to cloud.");
wifiReady = true;
Particle.connect();
connectingToCloud = true;
}

// Once we have connected, or tried to connect, then we ask for our name, if we havnt got it.
if(connectingToCloud && !waitingForName && (nReadingsSaved == 0 || !haveGotName) && waitFor(Particle.connected, 5000))
{
//Serial.println("We dont have the name will register with the cloud...");
Particle.subscribe("particle/device/name", handler);
Particle.publish("particle/device/name");
//Serial.println("Waiting to get device name...");
waitingForName = true;
cloudReady = true;
wifiReady = true;
}

if (connectingToCloud && !cloudReady) {
cloudReady = waitFor(Particle.connected, 5000);
}

// If we have our name.. we can upload the data!
if (cloudReady && haveGotName) {
// we have waited for some time... now to do something...
if (wifiReady && cloudReady) {
// We are connected to the internet! lets dance!
//Serial.println("We are connected, now to upload!!");
publishMyData();
//Serial.println("Finsihed loop, " + String(nReadingsSaved) + "Now time for sleep...");
if (nReadingsSaved >= nSaves) {
nReadingsSaved = 0;
}
goToSleep();

}
}
} else { //This is not a cloud loop..
saveSensorInfo();
//Serial.println("Finsihed loop " + String(nReadingsSaved) + ". Not a cloud loop, so time for sleep...");
nReadingsSaved++;
if (nReadingsSaved >= nSaves) {
nReadingsSaved = 0;
}
goToSleep();
*/
