/*
 * Project Boron3rdPartySim
 * Description:
 * Author:
 * Date:
 */

bool toogle = false;
bool isDebugMode = true;

FuelGauge fuel;

// setup() runs once, when the device is first turned on.
void setup() {
  // Put initialization like pinMode and begin functions here.
  
  Serial.begin(9600);
    pinMode(D7, OUTPUT);
    digitalWrite(D7, HIGH);
}

// loop() runs over and over again, as quickly as it can execute.
void loop() {
  // The core of your code will likely live here.

    if(toogle)
    {
    digitalWrite(D7, HIGH);
    toogle= false;
    }
    else
    {
     digitalWrite(D7, LOW);
      toogle= true;
   }

    Serial.println("Running loop, ");
    SimType simType = Cellular.getActiveSim();
    Serial.printlnf("simType=%d", simType);
   
Serial.println("Fuel volatage= " + String(fuel.getVCell()) );
Serial.println("Fuel State of Charge= " + String(fuel.getSoC()) );
  

    delay(1000);
}